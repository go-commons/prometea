<center>

# The first commercial open CNC plasma kit


<iframe width="100%" height="450" src="https://www.youtube.com/embed/1gGuKwr-dK0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# A metal cutting machine for the new era of agile and distributed manufacturing 🔥

## Machine overview

# 👇

![](./images/render-cnc.jpg)


<center>

</center>

<!-- <h1 align="center"> Polemidos</h1> -->







### Makers spaces, schools, Universities, small business and farms need fabrication tools

### Farmers and makers want to test and sell the products they design

### We are moving towards a just in time and on demand economy that is localized, efficient and will create new jobs

</br>

# Key product features



### **Open Hardware Technology** 📜

### **Ready to assemble kit** 🔩

### **Accessible price**	$3000 👍

### **Online tutorials and training sessions** 📖

### **Open designs library** premium plan

### **cutting thickness:** 1/2 in

### **cutting surface:** 4x8 feet


</center>
