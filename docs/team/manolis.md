<center>

<img :src="$withBase('/manolis.png')" alt="eric" width="20%">

# Manolis
## Lead developer, Product Owner
### 10 years of work experience

</center>

Manolis is a renaissance man, he has build and design complex equipment for his farm. He has a user perspective and has developed engineering skills to carry on with his self sufficiency philosophy. He is a farmer-hacker-engineer and designer.

> I started my own farm where I design and build everything from scratch. I have worked in almost every position in the residential construction field. I am self taught on CAD design  and I spend my free time researching and learning whatever is needed in order to move my farm forward, which led me to design a few machines from scratch, including the 1st prototypes of grapple,the cnc plasma, and the firewood processor.

#### Contact details
manolis@opensourcetools.org
