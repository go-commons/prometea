<center>

<img :src="$withBase('/eric.png')" alt="eric" width="20%">

# Eric
## Automation Engineer
## 15 years of work experience

</center>

Eric is a highly skilled automation engineer. He has been automating from autonomous flying drones, to old milling machines. He is also an inventor, maker and entrepreneur. He has build many drones, developed test benches for drone motors, and is also a maker equipped with CAD skills, electronics and software.
