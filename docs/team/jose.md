<center>

<img :src="$withBase('/jose.png')" alt="eric" width="20%">

# Jose
## UX & Product Designer
## +6 years of experience, TU Delft graduate

</center>


I am a UX and strategy driven person. I focus on business feasibility, scale up numbers, investment and service driven design. I am also fascinated by sustainability, open  hardware and community led innovations. Peer to peer technologies are changing the world faster than what we can understand it, we need to design and be prepare for this emerging context. Besides this I have a set of classic engineering skills, CAD, drawing, project management, and also programming.


#### Contact details
jose.urra86@gmail.com
