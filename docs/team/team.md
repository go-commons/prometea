<center>

# The team

<img :src="$withBase('/team.png')" alt="team" width="100%">

</center>

## A multidisciplinary and diverse crew of experienced, young and passionate guys

### We have covered all the departments we think is needed to carry on with this vision:
- Hardware engineering and automation engineering
- User Experience Design
- Business model design and development
- The maker, hacker and inventing culture
- Community centered service design
