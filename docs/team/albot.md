<center>

<img :src="$withBase('/albot.png')" alt="eric" width="20%">

# Dimitru ALBOT
## Design Engineer, Maker, Educator
## 2+ years experience in digital manufacturing and education

</center>

MIT graduate. Former FabLab coordinator in Jordan, currently full-time instructor of Fab Academy in Germany, mentoring the "How to Make (almost) Anything" course run by Neil Gershenfeld, director of MIT's Center for Bits and Atoms.

Dumitru earned a bachelor degree in Environment & Energy from Rhine-Waal University in Germany; successfully graduated from Fab Academy 2017; acquired New Venture Leadership certificate by completing the MIT Innovation and Entrepreneurship bootcamp in Brazil
