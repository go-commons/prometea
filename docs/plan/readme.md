# Roadmap
## December 2019: First commercial open plasma cut kit
*We are bringing to the market the first safe, accessible, easy to assemble, ready to use and open **metal cutting machine** for the new generation of industrialists in the 21st century.*

This is first step will focus on having a ready to market open hardware and automated plasma cutting kit to democratize manufacturing.

### Promises
- Training services and tutorials on CAD/CAM, business related issues.
- Open Designs to start cutting and using your plasma cutter.

## May 2020: Market place for metal cut design parts and components
We want to launch our platform services

### Promises
*We live in the age of platforms, service oriented enterprise and peer-to-peer production, hardware is only one of the very important components of this puzzle.*

- Designers and engineers will be able to publish and distribute their designs under different licenses.  
- Owners of metal cutting equipment can sell their parts on demand.
- Buyers can connect to sellers easily and also request new designs to be made and fabricated.

## May 2021: Product diversification and platform extension
*With the power of AI we will be able easily help our customers and users in getting better quotes, make better choices with regard to time, costs and also market segmentation.*

### Promises:
- New automated devices for metal fabrication
- APIs
- Customizable designs that automatically generate BOMs and cost estimations
- Easy and free to use CAD service to generate and share designs
- Quotes and automatic CAD/CAM generation
