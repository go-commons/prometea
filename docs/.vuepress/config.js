module.exports = {
  title: "Prometea",
  dest: "public",
  base: "/prometea/",
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }]
  ],

  themeConfig: {
    logo: 'logo.png',
    // projectLogo: "/logo.png",
    projectTitle: "Polemidos",
    sidebarDepth:0 ,
    sidebar: [
      {
        title: 'Product',
        collapsable: false,
        children: [
          ['/', 'Product Intro'],
          ['/product/prototype', 'Validated Prototype'],
          // ['/product/vision_v','The Product Platform Vision'],
          // ['/product/features','Product features'],
          ['/product/roadmap', 'Our five year Roadmap ']
        ]
      },
      {
        title: 'Support the project',
        collapsable: false,
        children: [
          ['/market/survey','Fill in our survey'],
          // ['/market/invest','Invest on us']

        ]
      },
      {
        title:'The team',
        collapsable: false,
        children:[
          ['/team/team', 'The team'],
          ['/team/manolis','Manolis'],
          ['/team/albot','Dimitru'],
          ['/team/eric','Eric'],
          ['/team/jose','Jose']
        ]
      },
      // {
      //   title:'Partners',
      //   collapsable: false,
      //   children:[/*...*/]
      // }
    ]
    }

  };
