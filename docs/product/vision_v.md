## Prometea is a platform, not just a machine
### This is how the concept works
With **Prometea** customers can have also access to new designs, tutorials and create new designs to sell on our platform.

These customers now become partners and can sell via our services their machine parts and connect with potential customers close to their location.

End customers can now find open designs that solve their needs, improve them and buy them from local suppliers that are part of Prometea's network.


## :+1: You got it! Prometea is also a emergent and distributed market place
