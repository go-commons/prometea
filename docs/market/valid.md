<center>

# This is what makers think about our value proposition

</center>

![Slide](../images/01_Slide.png)

**100% of our customers are looking for an accessible plasma cutter kit**

**76% wants this machine to build custom parts** they need for their projects


**70% wants to see the  Bill of Materials** so that they know what they are paying for

![Open Source](../images/02_OpenSource.png)
**84% wants an open source product** to study and hack the technology
