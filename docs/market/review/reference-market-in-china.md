Industry influence factor

First, favorable factors

(1) The policy is good

On January 28, 2015, Li Keqiang presided over the State Council executive meeting to study and determine the policy measures to support the development of the maker space and promote the public innovation and entrepreneurship. The Central Document first mentioned the “creator space”. On March 2, 2015, the General Office of the State Council issued the “Guiding Opinions on Developing Maker Space to Promote Public Innovation and Entrepreneurship”, marking the first deployment of the “Creator Space” platform at the national level to support the public innovation and entrepreneurship. The "Opinions" proposes that by 2020, a number of new entrepreneurial service platforms will be formed, such as the maker space that effectively meets the needs of the masses for innovation and entrepreneurship, has strong professional service capabilities, and is characterized by low cost, convenience, and openness.

(2) Double-creative economy

At present, China is undergoing supply-side reforms, and the dual-invasion economy driven by “Internet Plus” has become the engine of China’s economic growth. On March 5, 2016, the government work reports of the two associations repeatedly mentioned “mass entrepreneurship, innovation” and raised it to the height of one of the “dual engines” of China’s economic transformation and growth, showing the social double Create an economic focus.

(3) High entrepreneurial enthusiasm

The call for “mass entrepreneurship and innovation” has been proposed for two years, and the entrepreneurial heat wave has continued to heat up. On the one hand, the Chinese people's entrepreneurial willingness ranks first in the world, and 85% of the people show a strong willingness to start a business. On the other hand, the number of new entrepreneurs and new companies continues to grow. Among them, about 80% of the newly registered enterprises are distributed in the tertiary industry, and the “Internet +” related industries have become hot spots for entrepreneurship.

(4) Relying on cities, universities, science and technology parks, etc.

1. The cities such as “Northern Guangzhou-Shenzhen” have strong scientific research strength and developed economy. The information technology and intelligent manufacturing and processing supporting capabilities are strong. The government supports the government and the event is also active. For example, Beijing Chuangke Space was awarded the license of Zhongguancun Innovation Incubator, enjoying the support of taxation, housing leasing funds and business incubator services; Shenzhen district governments rushed to layout and set up a number of maker space carriers for Shenzhen Maker and its development. The creative and innovative projects provide technical support, platform support and industrial support.

2. Hangzhou Onion Capsule and other maker spaces rely on universities and technology parks. The scientific research resources in colleges and universities and science parks are in good condition, and teachers, students' professions and special interests can be brought into play, which has triggered the germination and development of college makers' space.

Second, the unfavorable factors

(1) Low occupancy rate

At present, the domestic private space occupancy rate is not enough (80% of the space occupancy rate is less than 30%), resulting in no space to screen more quality projects, which directly leads to insufficient funds. According to the industry's judgment, a single space occupancy rate of more than 60% can achieve breakeven, but most of the space does not reach this ratio.

According to the “2015 Xiamen Urban Innovation Space Development White Paper” issued by the Chuangchuang Space Industry Association of Xiamen, the space utilization rate of Xiamen Zhongchuang is only 51%. Compared with it, Zhongguancun has created an average occupancy rate of about 60%.

(2) Zhongchuang becomes a renting house

The creation space is backward, about 80% of the creation space is still providing office space for entrepreneurs, only about 20% can provide supporting registration, legal, taxation, financing, promotion and other services, and in this 20% of the creation In space, the platform that provides high-quality, full-featured services is almost less than 1%. This means that many project teams that have settled in the space are just finding a place to work, rather than gaining a platform to foster entrepreneurial ideas. This large-scale backward business is not conducive to the upgrading and development of innovation and entrepreneurship.

(3) Fuzzy profit model

In 2016, after the spurt development in 2015, Zhongchuang Space has some space because it has few teams, no profit model, and it is difficult to escape the fate of bankruptcy. In the boom of mass entrepreneurship and innovation, the new incubators such as Zhongchuang Space have grown rapidly, which is a good thing for start-ups. However, there has also been a bubble behind the rapid growth. The conceptualization of the overwhelming, lack of clear and feasible industry standards, the professional quality of the operating entities and the unevenness of their capabilities are all restricting the sustainable development of the space. Most of the space is created by rent as an important source of profit, or even the only source of profit. In the policy support period, it is still barely viable, and once the subsidy is lost, it may be difficult to sustain. If there is no good profit model, no good projects and teams, the big reshuffle of the creation space is inevitable.

Market size forecast

In 2014-2015, the scale of China's creative space grew from more than 50 to more than 2,300, an increase of 46 times. By the end of 2016, the number of public spaces may exceed 4,000. CIC Consulting Industry Research Center predicts that the number of China's Chuangchuang space will reach 5,320 in 2017, and the compound annual growth rate will be 21.62% in the next five years (2017-2021). In 2021, the number of space created by China will reach 11 , 640.
